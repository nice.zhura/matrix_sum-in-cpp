#include <iostream>
#include <stdexcept>
#include <vector>

class Matrix {
private:
    // matrix variable
    std::vector<std::vector<int>> matrix;

    // sizes of matrix
    int num_rows_;
    int num_cols_;

public:
    // initial constructors
    Matrix();
    Matrix(int num_rows, int num_cols);

    // reset method
    void Reset(int num_rows, int num_cols);

    // at methods
    int At(int num_rows, int num_cols) const;
    int& At(int num_rows, int num_cols);

    // get sizes of matrix
    int GetNumRows() const;
    int GetNumColumns() const;

    // friend operators == and +
    friend bool operator==(const Matrix& one, const Matrix& two);
    friend Matrix operator+(const Matrix& one, const Matrix& two);

    // friend operators << and >>
    friend std::ostream& operator<<(std::ostream& out, const Matrix& matrix);
    friend std::istream& operator>>(std::istream& in, Matrix& matrix);
};

Matrix::Matrix() {
    num_rows_ = 0;
    num_cols_ = 0;
}

Matrix::Matrix(int num_rows, int num_cols) {
    Reset(num_rows, num_cols);
}

void Matrix::Reset(int num_rows, int num_cols) {
    if (num_rows < 0) {
        throw std::out_of_range("num_rows must be >= 0!");
    }
    if (num_cols < 0) {
        throw std::out_of_range("num_cols must be >= 0!");
    }
    if (num_rows == 0 || num_cols == 0) {
        num_rows = num_cols = 0;
    }

    num_rows_ = num_rows;
    num_cols_ = num_cols;

    matrix.assign(num_rows, std::vector<int>(num_cols));
}

int Matrix::At(int num_rows, int num_cols) const {
    return matrix.at(num_rows).at(num_cols);
}

int& Matrix::At(int num_rows, int num_cols) {
    return matrix.at(num_rows).at(num_cols);
}

int Matrix::GetNumRows() const {
    return num_rows_;
}

int Matrix::GetNumColumns() const {
    return num_cols_;
}

bool operator==(const Matrix& one, const Matrix& two) {
    if (one.GetNumColumns() != two.GetNumColumns()) {
        return false;
    }

    if (one.GetNumRows() != two.GetNumRows()) {
        return false;
    }

    for (int row = 0; row < one.GetNumRows(); ++row) {
        for (int col = 0; col < one.GetNumColumns(); ++col) {
            if (one.At(row, col) != two.At(row, col)) {
                return false;
            }
        }
    }

    return true;
}

Matrix operator+(const Matrix& one, const Matrix& two) {
    if (one.GetNumColumns() != two.GetNumColumns()) {
        throw std::invalid_argument("Mismatched number of rows!");
    }

    if (one.GetNumRows() != two.GetNumRows()) {
        throw std::invalid_argument("Mismatched number of columns!");
    }

    Matrix result(one.GetNumRows(), one.GetNumColumns());
    for (int row = 0; row < one.GetNumRows(); ++row) {
        for (int col = 0; col < one.GetNumColumns(); ++col) {
            result.At(row, col) = one.At(row, col) + two.At(row, col);
        }
    }

    return result;
}

std::ostream& operator<<(std::ostream& out, const Matrix& matrix) {
    out << matrix.GetNumRows() << ' ' << matrix.GetNumColumns() << std::endl;
    for (int row = 0; row < matrix.GetNumRows(); ++row) {
        for (int col = 0; col < matrix.GetNumColumns(); ++col) {
            out << matrix.At(row, col) << ' ';
        }
        out << std::endl;
    }
    return out;
}

std::istream& operator>>(std::istream& in, Matrix& matrix) {
    int num_rows, num_cols;
    in >> num_rows >> num_cols;

    matrix.Reset(num_rows, num_cols);
    for (int row = 0; row < matrix.GetNumRows(); ++row) {
        for (int col = 0; col < matrix.GetNumColumns(); ++col) {
            in >> matrix.At(row, col);
        }
    }
    return in;
}

int main() {
    // Initialize Matrices
    Matrix one;
    Matrix two;

    // Read Matrices from cin stream
    std::cin >> one >> two;

    // Print the sum of matrices into cout stream
    std::cout << (one + two) << std::endl;

    return 0;
}
